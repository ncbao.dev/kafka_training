using Cassandra;
using Microsoft.AspNetCore.Mvc;

namespace KafkaTraining.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly Cassandra.ISession _cassandraSession;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, Cassandra.ISession cassandraSession)
        {
            _logger = logger;
            _cassandraSession = cassandraSession;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            //Console.WriteLine("Connected to cluster: " + cluster.Metadata.ClusterName);
            var keyspaceNames = _cassandraSession
                                .Execute("SELECT * FROM system_schema.keyspaces")
                                .Select(row => row.GetValue<string>("keyspace_name"));
            Console.WriteLine("Found keyspaces:");
            foreach (var name in keyspaceNames)
            {
                Console.WriteLine("- {0}", name);
            }
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}