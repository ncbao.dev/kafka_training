﻿using Cassandra;
//using Cassandra.Data;
using EvolveDb;

namespace KafkaTraining.Database
{
    public class CassandraHelper
    {
        private readonly Cassandra.ISession _session;
        private readonly ILogger<CassandraHelper> _logger;

        public CassandraHelper(Cassandra.ISession session, ILogger<CassandraHelper> logger)
        {
            _session = session;
            _logger = logger;
        }

        public async Task MigrateDatabase()
        {
            //var batch = new BatchStatement().SetBatchType(BatchType.Logged)
            //                .Add(new SimpleStatement("CREATE TABLE IF NOT EXISTS test_table(testid int PRIMARY KEY, testname text)"));

            var rowSet = await _session.ExecuteAsync(new SimpleStatement("CREATE TABLE IF NOT EXISTS test_table(testid int PRIMARY KEY, testname text)")).ConfigureAwait(false);

            if(rowSet.Info.IsSchemaInAgreement)
            {
                _logger.LogInformation("Migration successfully.");
            }
            else
            {
                _logger.LogError("Migration failed.");
            }
        }
        //private static CqlConnection _connection;                           
        //private static ILogger<CassandraHelper> _logger;

        //public static async Task Initialize(CqlConnection connection)
        //{
        //    _connection = connection;
        //    _logger = LoggerFactory.Create(builder => builder.AddConsole()).CreateLogger<CassandraHelper>();
        //    await Task.CompletedTask;
        //}

        //public static async Task MigrateDatabase()
        //{
        //    try
        //    {
        //        //Console.WriteLine(_connection.ConnectionString);
        //        var evolve = new Evolve(_connection, message => _logger.LogInformation(message))
        //        {
        //            Locations = new[] { "Database/Migrations" },
        //            IsEraseDisabled = true,
        //        };

        //        evolve.Migrate();
        //        await Task.CompletedTask;
        //    } 
        //    catch(Exception ex)
        //    {
        //        _logger.LogError("Database migration failed.", ex);
        //        throw;
        //    }
        //}
    }
}
