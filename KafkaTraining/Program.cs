﻿using KafkaTraining.Database;
using KafkaTraining.Settings;
using System.Net;

var builder = WebApplication.CreateBuilder(args);

//var clusterConfig = builder.Configuration.GetSection(nameof(CassandraSetting)).Get<CassandraSetting>();
//var nodeEndpoints = clusterConfig.ContactPoints.Select(x => new IPEndPoint(IPAddress.Parse(x.IPAddress), x.Port)).ToList();
//var cassandraConnectionString = new Cassandra.CassandraConnectionStringBuilder
//{
//    ClusterName = clusterConfig.ClusterName,
//    ContactPoints = new string[] { nodeEndpoints[0].Address.ToString() },
//    DefaultKeyspace = clusterConfig.Keyspace,
//    Port = nodeEndpoints[0].Port,
//    Username = clusterConfig.Username,
//    Password = clusterConfig.Password,
//}.ConnectionString;

//// Inject connection
//await CassandraHelper.Initialize(new Cassandra.Data.CqlConnection(cassandraConnectionString));
//// Migrate Database
//await CassandraHelper.MigrateDatabase();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<Cassandra.ISession>((services) =>
{
    var clusterConfig = builder.Configuration.GetSection(nameof(CassandraSetting)).Get<CassandraSetting>();
    //var nodeEndpoints = clusterConfig.ContactPoints.Select(x => new IPEndPoint(IPAddress.Parse(x.IPAddress), x.Port)).ToList();
    var nodeEndpoints = clusterConfig.ContactPoints.Select(x => new IPEndPoint(Dns.GetHostEntry(x.IPAddress).AddressList[0], x.Port)).ToList();
    var cassandraConnectionString = new Cassandra.CassandraConnectionStringBuilder
    {
        ClusterName = clusterConfig.ClusterName,
        ContactPoints = new string[] { nodeEndpoints[0].Address.ToString() },
        DefaultKeyspace = clusterConfig.Keyspace,
        Port = nodeEndpoints[0].Port,
        Username = clusterConfig.Username,
        Password = clusterConfig.Password,
    }.ConnectionString;
    return Cassandra.Cluster.Builder()
                    .WithConnectionString(cassandraConnectionString)
                    .AddContactPoints(nodeEndpoints.Skip(1).ToList())
                    .Build().Connect();
});

var app = builder.Build();

// Before startup
await new CassandraHelper(app.Services.GetService<Cassandra.ISession>()!, app.Services.GetService<ILogger<CassandraHelper>>()!).MigrateDatabase();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();