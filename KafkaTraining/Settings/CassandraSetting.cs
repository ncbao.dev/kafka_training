﻿namespace KafkaTraining.Settings
{
    public class CassandraSetting
    {
        public string ClusterName { get; set; }
        public List<CassandraContactPoints> ContactPoints { get; set; }
        public string Keyspace { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }                                       
                                            
    public class CassandraContactPoints
    {
        public string IPAddress { get; set; }
        public int Port { get; set; }
    }
}
